#!/usr/bin/env sh

set -eu

exitTrap() {
  logEventEnd 'Container init script'
  exit 0
}

createTraps() {
  trap exitTrap WINCH QUIT
}

backupInstallScript() {
  process='Backup install script'
  logEventStart "${process}"
  install -d -m 0750 -o www-data -g www-data /var/src
  if [ -d /var/www/html/install/ ]; then
    cp -r /var/www/html/install/ /var/src
  elif [ -d /var/src/install ]; then
    cp -r /var/src/install /var/www/html/
  fi
  logEventEnd "${process}"
}

createFilesSubdirectories() {
  process='Create files subdirectories'
  logEventStart "${process}"
  directories="
    _cache
    _cron
    _dumps
    _graphs
    _lock
    _log
    _pictures
    _plugins
    _rss
    _sessions
    _tmp
    _uploads
    "
  for directory in ${directories}; do
    if [ ! -d "files/$directory" ]; then
      mkdir "files/$directory"
    fi
  done
  logEventEnd "${process}"
}

clearTmpFiles() {
  process='Clean temporary files'
  logEventStart "${process}"
  find files/_cache -mindepth 1 -maxdepth 1 -exec rm -r {} +
  find files/_tmp -mindepth 1 -maxdepth 1 -exec rm -r {} +
  logEventEnd "${process}"
}

chownFiles() {
  process='Set owner for config and files directories'
  logEventStart "${process}"
  chown www-data: config files -R
  logEventEnd "${process}"
}

prepareInstance() {
  backupInstallScript
  createFilesSubdirectories
  clearTmpFiles
  chownFiles
}

waitDatabase() {
  process='Wait for database is ready'
  logEventStart "${process}"
  php /usr/local/bin/support/wait-database.php &
  wait $!
  logEventEnd "${process}"
}

getDatabaseAutoInstallStatus() {
  process='Check if database is ready for install or update'
  logEventStart "${process}"
  status=0
  php /usr/local/bin/support/check-database.php &
  wait $! || status=$?
  logEventEnd "${process}"
  return $status
}

installDatabase() {
  process='Install database'
  logEventStart "${process}"
  if [ -f config/config_db.php ]; then
    rm config/config_db.php
  fi
  su www-data -s /bin/sh -c install-glpi
  logEventEnd "${process}"
}

updateDatabase() {
  process='Update database'
  logEventStart "${process}"
  tee config/config_db.php <<PHP >/dev/null
<?php
class DB extends DBmysql {
    public \$dbhost     = '${MYSQL_HOSTNAME}';
    public \$dbuser     = '${MYSQL_USER}';
    public \$dbpassword = '${MYSQL_PASSWORD}';
    public \$dbdefault  = '${MYSQL_DATABASE}';
}
PHP
  chown www-data: config/config_db.php
  su www-data -s /bin/sh -c update-glpi
  logEventEnd "${process}"
}

doAutoInstall() {
  waitDatabase
  status=0
  getDatabaseAutoInstallStatus || status=$?
  case "${status}" in
  "1")
    installDatabase
    ;;
  "2")
    updateDatabase
    ;;
  *)
    logMessage 'Database is not ready for auto install'
    return
    ;;
  esac
  remove-install
}

checkAutoInstall() {
  case "${AUTO_INSTALL}" in
  [YyTt1]*)
    logMessage 'Auto install is enabled'
    doAutoInstall
    ;;
  default)
    logMessage 'Auto install script is disabled'
    ;;
  esac
}

# shellcheck disable=SC1091
main() {
  . /usr/local/bin/support/environment-variables
  . /usr/local/bin/support/log-events

  createTraps
  prepareInstance
  checkAutoInstall
}

main
